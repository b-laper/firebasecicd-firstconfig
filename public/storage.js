import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
import {
  getStorage,
  ref,
  uploadBytesResumable,
  getDownloadURL,
  listAll,
  deleteObject,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-storage.js";

const firebaseConfig = {
  apiKey: "AIzaSyCktReI4qAXdmAZXqrn98-FuvAGmPdSTTM",
  authDomain: "fir-storage-86838.firebaseapp.com",
  projectId: "fir-storage-86838",
  databaseURL:
    "https://fir-storage-86838-default-rtdb.europe-west1.firebasedatabase.app",
  storageBucket: "fir-storage-86838.appspot.com",
  messagingSenderId: "440098369290",
  appId: "1:440098369290:web:068ef79bf1f20c9ce7d4f3",
  measurementId: "G-EXGWD2K51N",
};
// app init
const app = initializeApp(firebaseConfig);
const storage = getStorage(app);

// Mapowanie elementu z interfejsu usera
const querySelectors = {
  imgNameInput: document.querySelector("#imgNameInput"),
  imgExtension: document.querySelector("#imgExtension"),
  btnSelectImg: document.querySelector("#btnSelectImg"),
  btnUploadImg: document.querySelector("#btnUploadImg"),
  imgBox: document.querySelector("#imgBox"),
  select: document.querySelector("#select"),
  uploadProgressBar: document.querySelector("#uploadProgressBar"),
  allFilesList: document.querySelector("#allFilesList"),
  btnGetFiles: document.querySelector("#btnGetFiles"),
  btnOneFile: document.querySelector("#btnOneFile"),
  imageStorage: document.querySelector("#imageStorage"),
  showImgBtn: document.querySelector("#showImgBtn"),
  showImg: document.querySelector("#showImg"),
  userNameInput: document.querySelector("#userNameInput"),
  btnGetImgForUser: document.querySelector("#btnGetImgForUser"),
  //delete
  userImage: document.querySelector("#userImage"),
  inputDeleteName: document.querySelector("#inputDeleteName"),
  deleteFromRealTimeDB: document.querySelector("#deleteFromRealTimeDB"),
  //update
  inputUpdateName: document.querySelector("#inputUpdateName"),
  updateBtn: document.querySelector("#updateBtn"),
  fileToUpdateName: document.querySelector("#fileToUpdateName"),
  selectUpdateImg: document.querySelector("#selectUpdateImg"),
  userList: document.querySelector("#userList"),
  offlineDiv: document.querySelector("#offline"),
};
// zmienne pomocnicze do przechowywania i wrzucania plikow

const fileReader = new FileReader();

let files = [];

//proces wyboru pliku
const input = document.createElement("input");
input.type = "file";

input.onchange = (e) => {
  files = e.target.files;
  fileReader.readAsDataURL(files[0]);

  const imageNameIntoParts = files[0].name.split(".");
  // [kot, jpg] Jak zabezpieczyc przed czyms takim a.a.b.c.jpg --> obstawiam regEx
  const imgName = imageNameIntoParts[0];
  const imgExt = `.${imageNameIntoParts[1]}`;
  console.log(files);

  querySelectors.imgNameInput.value = imgName;
  querySelectors.imgExtension.innerHTML = imgExt;
};

querySelectors.btnSelectImg.onclick = (e) => {
  input.click();
};

// Upload pliku do stroagu
querySelectors.btnUploadImg.onclick = async (e) => {
  const imageToUpload = files[0];
  const imageFullName = imgNameInput.value + imgExtension.innerHTML;

  const metaData = {
    contentType: imageToUpload.type,
  };

  // Komunikacja ze storage w celu dodania pliku

  //referencja do storage
  const storageRef = ref(storage, "images/" + imageFullName);

  const uploadTask = uploadBytesResumable(storageRef, imageToUpload, metaData);

  // zamiast statechanged mozna by było uzyc wartosci z modułu TaskEvent
  // taskEvent
  uploadTask.on(
    "state-changed",
    (snapshot) => {
      const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      uploadProgressBar.innerHTML = progress + "% done";
    },
    (error) => console.error(error),
    () => {
      getDownloadURL(uploadTask.snapshot.ref).then((url) => {
        addToRealTimeDB(url);
        querySelectors.imgBox.setAttribute("src", url);
      });
    }
  );
};
//pobranie wszystkcih dostepnych plików (np img)
const imgFolderRef = ref(storage, "images/");

// Delete the file
const deleteImg = (imgToDelete) => {
  deleteObject(imgToDelete)
    .then(() => {
      console.log("FileDeleted");
    })
    .catch((error) => {
      console.warn(error);
    });
};

querySelectors.btnGetFiles.onclick = async () => {
  allFilesList.innerHTML = "";
  listAll(imgFolderRef)
    .then((list) => {
      list.items.forEach((fileRef) => {
        // let option = document.createElement("option");
        // option.innerHTML = `<option> ${fileRef}</option>`;
        // querySelectors.select.appendChild(option);
        allFilesList.innerHTML += ` ${fileRef}<button class="deleteImg"> Delete </button> <br/>`;
        // Create a reference to the file to delete
        const imgRefDelete = ref(storage, fileRef._location.path);
        const delButtons = document.querySelectorAll(".deleteImg");
        delButtons.forEach((button) => {
          button.addEventListener("click", () => deleteImg(imgRefDelete));
        });
      });
    })
    .catch((error) => {
      console.warn(error);
    });
};

//pobranie konkretnego obrazka

// tworzymy sciezke do awataru w bazie danych z userem  zamiast "images/----plik----"

const imageRef = ref(storage, `images/alain1-large.jpg `);
const getImgFile = async (file) => {
  getDownloadURL(file)
    .then((url) => {
      const xhr = new XMLHttpRequest();
      xhr.responseType = "blob";
      xhr.onload = (event) => {
        const blob = xhr.response;
      };
      xhr.open("GET", url);
      xhr.send();
      querySelectors.imageStorage.setAttribute("src", url);
    })
    .catch((error) => {
      console.warn(error);
    });
};

querySelectors.showImgBtn.addEventListener(
  "click",
  () => getImgFile(ref(storage, `images/${showImg.value}`)) //ZMIENIĆ na SELECT Z LISTY
);
querySelectors.btnOneFile.addEventListener("click", () => getImgFile(imageRef));

//------------------------******************** RealTime *************************---------------
// dodawanie recordu do real time database
import {
  getDatabase,
  set,
  ref as realTimeDBref,
  onValue,
  remove,
  child,
  get,
  push,
  update,
  onDisconnect,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";
const realtimeDatabase = getDatabase();

///// uzupełnienie configu databaseURL: "https://fir-storage-86838-default-rtdb.europe-west1.firebasedatabase.app",

// funkcja wrzucajaca do bazy danych RT

const addToRealTimeDB = (imageUrl) => {
  const imageName = querySelectors.imgNameInput.value;
  // const imageExtension = querySelectors.imgExtension.innerHTML;

  set(realTimeDBref(realtimeDatabase, "users/" + imageName), {
    username: imageName,
    imageUrl: imageUrl,
  });
};

// odczyt z rt

const getImageUrlFromRealTimeDB = () => {
  const imageNameRequest = userNameInput.value;
  const imageRef = realTimeDBref(realtimeDatabase, "users/" + imageNameRequest);

  onValue(imageRef, (snapshot) => {
    const fullData = snapshot.val();

    // pokazanie awataru uzytkownika

    console.log(fullData, fullData.username);
    querySelectors.userImage.setAttribute("src", fullData.imageUrl);
  });
};

//obsługa pobierania awataru
querySelectors.btnGetImgForUser.onclick = async () => {
  getImageUrlFromRealTimeDB();
};
const dbRef = getDatabase();
console.log(dbRef);
const refToDel = {
  imageUrl:
    "https://firebasestorage.googleapis.com/v0/b/fir-storage-86838.appspot.com/o/images%2Fjohny.jpg?alt=media&token=3a4689a2-a1c3-47b5-a480-d47f126e0a28",
  username: "johny",
};
const newRef = realTimeDBref(dbRef, "users/johny");

// set(realTimeDBref(dbRef, "users/" + refToDel.username), {
//   username: "johny",
//   imageUrl: refToDel.imageUrl,
// })
//   .then(() => {
//     "OK";
//   })
//   .catch((error) => {
//     // The write failed...
//   });

//usuwanie rekordu z db

//

// deleteRef();

querySelectors.deleteFromRealTimeDB.addEventListener("click", () => {
  const inputDelValue = querySelectors.inputDeleteName.value;
  const newRef = realTimeDBref(dbRef, `users/${inputDelValue}`);
  remove(newRef);

  console.log(newRef, "deleted");
});

const inputDelValue = querySelectors.inputDeleteName.value;

//update rekordu

// function updateAvatar() {
//   const db = getDatabase();
//   const user = querySelectors.inputUpdateName.value;
//   const image = querySelectors.fileToUpdateName;
//   // A post entry.
//   const postData = {
//     username: "vidar",
//     imageUrl:
//       "https://firebasestorage.googleapis.com/v0/b/fir-storage-86838.appspot.com/o/images%2Fv.jpg?alt=media&token=e16a641d-06fd-4714-8af4-3cc0d9ec7b9b",
//   };

//   const newData = {
//     users: {
//       vidar: {
//         imageUrl:
//           "https://firebasestorage.googleapis.com/v0/b/fir-storage-86838.appspot.com/o/images%2Falain.jpg?alt=media&token=13beed86-8f3e-41db-b97f-9f2190365ec0/",
//       },
//     },
//   };

//   return update(realTimeDBref(db), newData);
// }
let file = [];

//proces wyboru pliku
const inputUpd = document.createElement("input");
inputUpd.type = "file";

inputUpd.onchange = (e) => {
  file = e.target.files;
  fileReader.readAsDataURL(file[0]);

  const imageNameIntoParts = file[0].name.split(".");
  const imgName = imageNameIntoParts[0];
  console.log(file);

  querySelectors.inputUpdateName.value = imgName;
  // querySelectors.imgExtension.innerHTML = imgExt;
};

querySelectors.selectUpdateImg.onclick = (e) => {
  inputUpd.click();
};

// Upload pliku do stroagu
querySelectors.updateBtn.onclick = async (e) => {
  console.log("click", file);
  const imageToUpload = file[0];
  const imageFullName = querySelectors.inputUpdateName.value + ".jpg";

  const metaData = {
    contentType: imageToUpload.type,
  };

  // Komunikacja ze storage w celu dodania pliku

  //Update dodanego avatara
  const storageRef = ref(storage, "images/" + imageFullName);
  const nameUpd = inputUpdateName.value;
  uploadBytesResumable(storageRef, imageToUpload, metaData);

  await getDownloadURL(ref(storage, `images/${nameUpd}.jpg`))
    .then((url) => {
      update(realTimeDBref(realtimeDatabase, "users/" + nameUpd), {
        username: nameUpd,
        imageUrl: url,
      });
    })
    .catch((error) => {
      console.warn(error);
    });
};
const readUsers = () => {
  querySelectors.userList.innerHTML = "";
  const usersList = realTimeDBref(realtimeDatabase, "users/");
  onValue(usersList, (snapshot) => {
    const data = snapshot.val();
    console.log(data);
    Object.values(data).forEach((user) => {
      querySelectors.userList.innerHTML += `<div class="user" >${user.username}
      <img class="userAvatar" src=${user.imageUrl}/>
      </div> `;
      console.log(user);
    });
  });
};
readUsers();

const presenceRef = realTimeDBref(realtimeDatabase, "disconnectmessage");
// Write a string when this client loses connection
onDisconnect(presenceRef).set("I disconnected!");

onDisconnect(presenceRef)
  .remove()
  .catch((err) => {
    if (err) {
      console.error("could not establish onDisconnect event", err);
    }
  });
const connectedRef = realTimeDBref(realtimeDatabase, ".info/connected");
onValue(connectedRef, (snap) => {
  if (snap.val() === true) {
    querySelectors.offlineDiv.style.display = "none";
    console.log("connected");
  } else {
    querySelectors.offlineDiv.style.display = "block";
    console.log("not connected");
  }
});
// Uruchom moduł Storage w Firebase. X
// Ustaw zasady bezpieczeństwa dla modułu Storage. X
// Dodaj ręcznie plik do Storage’a. X
// Utwórz folder na obrazki w ramach Storage’a. X
// Dodaj do aplikacji możliwość wyboru obrazka z dysku. X
// Dodaj do aplikacji pobieranie nazwy pliku i rozszerzenia pliku. X
// Dodaj do aplikacji uploadowanie plików do Storage’a. X
// Dodaj do aplikacji możliwość wyświetlenia pliku na podstawie nazwy obrazka wpisanego przez użytkownika. X
// Dodaj do aplikacji możliwość wyświetlania listy wszystkich obrazków w folderze z obrazkami. X
// ** Dodaj możliwość usuwania obrazków z Storage’a. (  X )
// ** Dodaj korzystanie z modułu TaskEvent zamiast ręcznego podawania nazwy eventu. (   ) NOPE YET
// ** Dodaj zabezpieczenie przed nazwami plików zawierającymi “.” w środku nazwy, np. a.b.c.jpg.(   ) NOPE YET
// ** Dodaj możliwość wrzucania obrazków do Storage’a tylko zalogowanym użytkownikom.(   ) NOPE YET
// *** Dodaj możliwość wrzucenia pliku audio oraz odsłuchania tego pliku po naciśnięciu (   ) NOPE YETwybranego przycisku.
// 15. Z pobranej listy wszystkich plików zrobić drop-down list i na podstawie tego wyświetlać (   ) NOPE YETobrazki.

// Uruchomić moduł Realtime Database w konsoli Firebase. X
// Dodanie nowych elementów UI w HTML. X
// Dopisanie funkcji, która w momencie uploadu obrazka do Storage’a będzie jednocześnie zapisywać w RealtimeDatabase informacji o użytkowniku oraz adresie URL avatara. X
// user : {
//     username: "imageName",
//     imageUrl: "imageUrl"
// }
// Umożliwość użytkownikowi pobranie avataru użytkownika na podstawie przekazanej nazwy.  X
// ** Dodaj możliwość usuwania rekordu dla danego użytkownika z Realtime Database. X
// ** Dodaj możliwość aktualizowania avatara użytkownika na podstawie username oraz nowego obrazka.  X
// *** Dodaj wyświetlanie listy wszystkich użytkowników oraz ich avatarów w formie username + avatar obok po prawej stronie. X Zmniejsz avatar w taki sposób, żeby obrazek obok nazwy był podobnej wielkości co rozmiar fontu.
// *** Dodaj label, który będzie informował, że użytkownik stracił połączenie z internetem. X

////// REFACTOR : update z listy uzytkowników , zmniejszenie funkcji , trochę styli , obsługa logowanych uzytkowinków
